package com.qcby.webServlet;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)//元注解:定义自定义注解的生命周期---》要在运行期间保留
@Target(value = {ElementType.TYPE})//元注解:定义自定义注解的作用范围 --》当前自定义注解只作用在类上（还有其他）
public @interface WebServlet {

    String urlMapping() default "";

}
