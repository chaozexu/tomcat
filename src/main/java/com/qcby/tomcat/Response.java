package com.qcby.tomcat;

import com.qcby.servlet.HttpServletResponse;
import com.qcby.util.FileUtil;
import com.qcby.util.ResponseUtil;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * @ClassName Response
 * @Operator czx
 * @Description TODO
 * @date 2022/6/17 21:23
 */

public class Response implements HttpServletResponse {

    //创建输出流
    private OutputStream outputStream;

    public Response(OutputStream outputStream){
        this.outputStream = outputStream;
    }

    @Override
    public void GetWrite(String count) throws IOException {
        outputStream.write(count.getBytes(StandardCharsets.UTF_8));
    }

    /**
     * 返回静态资源
     * @param path
     */
    public void GetWriteHtml(String path) throws Exception {
        //获取 resources 下边文件的路径
        String resourcesPath =  FileUtil.getResoucePath(path);
        File file = new File(resourcesPath);
        if(file.exists()){
            System.out.println("静态资源存在");
            FileUtil.writeFile(file,outputStream);
        }else {
            GetWrite(ResponseUtil.getResponseHeader404());
        }
    }
}
