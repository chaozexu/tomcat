package com.qcby.tomcat;

import com.qcby.servlet.HttpServletRequest;

/**
 * @ClassName Request
 * @Operator czx
 * @Description TODO
 * @date 2022/6/17 21:23
 */

public class Request implements HttpServletRequest {
    private String url;
    private String Method;

    public String getUrl() {
        return url;
    }

    public void setUrl(String urlMapping) {
        this.url = urlMapping;
    }

    @Override
    public String getMethod() {
        return Method;
    }

    @Override
    public void setMethod(String method) {
        Method = method;
    }
}
