package com.qcby.tomcat;

import com.qcby.conf.ServletConfigMapping;
import com.qcby.servlet.HttpServlet;
import com.qcby.util.ResponseUtil;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @ClassName MyTomcat
 * @Operator czx
 * @Description TODO
 * @date 2022/6/17 21:23
 */

public class MyTomcat {

    Request request = new Request();

    //获取类对象生成servlet对象
    public void disPath(Request request,Response response) throws InstantiationException, IllegalAccessException, IOException {
        Class<HttpServlet> httpServletClass = ServletConfigMapping.classMap.get(request.getUrl());
        if (httpServletClass != null){
            HttpServlet servlet = httpServletClass.newInstance();//生成servlet对象
            servlet.service(request,response);
        }
    }


    //启动tomcat的主方法
    public void starUp() throws Exception {
        ServletConfigMapping.initServlet();//启动阶段

        System.out.println("tomcat启动成功");
        //定义ServerSocket监听8080端口
        ServerSocket serverSocket = new ServerSocket(8080);

        while (true){
            Socket socket = serverSocket.accept();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        handler(socket);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }


    //解析用户请求数据
    public void handler(Socket socket) throws Exception{
        //获取输入数据
        InputStream inputStream = socket.getInputStream();
        getInput(inputStream);//解析HTTP请求

        //获取输出通道、获取输出流
        Response response = new Response(socket.getOutputStream());

        if (request.getUrl().equals("")){
            //返回404
            response.GetWrite(ResponseUtil.getResponseHeader404());
            System.out.println("404");
        }else if(ServletConfigMapping.classMap.get(request.getUrl())==null){
            //访问的是静态资源
            response.GetWriteHtml(request.getUrl());
            System.out.println("静态资源");
        }else {
            //访问的是动态资源
            disPath(request,response);
            System.out.println("动态资源");
        }



    }

    //解析HTTP请求
    public void getInput(InputStream inputStream) throws IOException {
        int count = inputStream.available();
        while (count == 0){
            count = inputStream.available();
        }
        byte[] bytes = new byte[count];
        inputStream.read(bytes);
        String content = new String(bytes);

        if (content.equals("")){
            System.out.println("这是一个空请求");
        }else {
            String firstLine = content.split("\\n")[0];
            request.setMethod(firstLine.split("\\s")[0]);
            request.setUrl(firstLine.split("\\s")[1]);
        }
    }

    public static void main(String[] args) throws Exception {
        MyTomcat tomcat = new MyTomcat();
        tomcat.starUp();
    }
}
