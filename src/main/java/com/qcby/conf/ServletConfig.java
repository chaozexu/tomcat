package com.qcby.conf;

/**
 * @ClassName ServletConfig
 * @Operator czx
 * @Description TODO
 * @date 2022/6/17 17:59
 *
 *
 * 获取类的信息
 */

public class ServletConfig {

    private String urlMapping;  //url
    private String classPath;//类的全路径名

    public ServletConfig(String urlMapping,String classPath){
        this.urlMapping = urlMapping;
        this.classPath = classPath;
    }

    public String getUrl() {
        return urlMapping;
    }

    public void setUrl(String url) {
        this.urlMapping = url;
    }

    public String getClassPath() {
        return classPath;
    }

    public void setClassPath(String classPath) {
        this.classPath = classPath;
    }

    @Override
    public String toString() {
        return "ServletConfig{" +
                "url='" + urlMapping + '\'' +
                ", classPath='" + classPath + '\'' +
                '}';
    }
}
