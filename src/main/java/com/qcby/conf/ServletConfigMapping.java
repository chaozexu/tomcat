package com.qcby.conf;

import com.qcby.servlet.HttpServlet;
import com.qcby.util.SearchClassUtil;
import com.qcby.webServlet.WebServlet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName ServletConfigMapping
 * @Operator czx
 * @Description TODO
 * @date 2022/6/17 21:07
 */

public class ServletConfigMapping {

    //定义servlet容器
    public static Map<String,Class<HttpServlet>> classMap = new HashMap<>();

    //将每个定义servlet的信息放入ServletConfig
    private static List<ServletConfig> configs = new ArrayList<>();


    static {
        //1.获取用户自定义的servlet的全路径名
        List<String> classPahts = SearchClassUtil.searchClass();
        for (String classPaht: classPahts){
            try {
                getMessage(classPaht);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    //利用反射获取主信息
    public static void getMessage(String classPath) throws ClassNotFoundException {
        Class clazz = Class.forName(classPath);
        //获取注解信息
        WebServlet webServlet = (WebServlet) clazz.getDeclaredAnnotation(WebServlet.class);

        //需要将解析的信息加载到configs结合当中----保证：url和类对象是相同的
        configs.add(new ServletConfig(webServlet.urlMapping(),classPath));
    }

    public static void initServlet() throws ClassNotFoundException {
        for (ServletConfig config: configs){
            classMap.put(config.getUrl(), (Class<HttpServlet>) Class.forName(config.getClassPath()));
        }
    }
}
