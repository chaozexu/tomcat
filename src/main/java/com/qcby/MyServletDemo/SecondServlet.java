package com.qcby.MyServletDemo;

import com.qcby.servlet.HttpServlet;
import com.qcby.servlet.HttpServletRequest;
import com.qcby.servlet.HttpServletResponse;
import com.qcby.webServlet.WebServlet;

@WebServlet(urlMapping = "/second")
public class SecondServlet extends HttpServlet {
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {

    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) {

    }
}
