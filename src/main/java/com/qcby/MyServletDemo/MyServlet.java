package com.qcby.MyServletDemo;

import com.qcby.servlet.HttpServlet;
import com.qcby.servlet.HttpServletRequest;
import com.qcby.servlet.HttpServletResponse;
import com.qcby.util.ResponseUtil;
import com.qcby.webServlet.WebServlet;

import java.io.IOException;

/**
 * @ClassName MyServlet
 * @Operator czx
 * @Description TODO
 * @date 2022/6/17 17:40
 *
 * 用户自定义的servlet类
 */

@WebServlet(urlMapping = "/my")
public class MyServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.GetWrite(ResponseUtil.getResponseHeader200("this is get"));
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.GetWrite(ResponseUtil.getResponseHeader200("this is post"));
    }
}
