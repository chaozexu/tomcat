package com.qcby.servlet;

import java.io.IOException;

/**
 * @ClassName HttpServletResponse
 * @Operator czx
 * @Description TODO
 * @date 2022/6/17 17:34
 */
public interface HttpServletResponse {

    public void GetWrite(String count) throws IOException;
}
