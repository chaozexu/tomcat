package com.qcby.servlet;

import java.io.IOException;

/**
 * @ClassName HttpServlet
 * @Operator czx
 * @Description TODO
 * @date 2022/6/17 17:32
 */

public abstract class HttpServlet {

    public abstract void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException;

    public abstract void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException;

    public void service(HttpServletRequest request,HttpServletResponse response) throws IOException {
        if("GET".equals(request.getMethod())){
            doGet(request,response);
        }else if("POST".equals(request.getMethod())){
            doPost(request,response);
        }
    }
}
