package com.qcby.servlet;

/**
 * @ClassName HttpServletRequest
 * @Operator czx
 * @Description TODO
 * @date 2022/6/17 17:33
 */
public interface HttpServletRequest {

    public String getMethod();

    public void setMethod(String method);
}
