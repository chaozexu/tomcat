package com.qcby.util;

/**
 * @ClassName ResponseUtil
 * @Operator czx
 * @Description TODO
 * @date 2022/6/17 17:42
 */


/**
 * 返回数据是应该添加的东西目的是让浏览器识别
 */
public class ResponseUtil {
    public  static  final String responseHeader200 = "HTTP/1.1 200 \r\n"+
            "Content-Type:text/html \r\n"+"\r\n";

    public static String getResponseHeader404(){
        return "HTTP/1.1 404 \r\n"+
                "Content-Type:text/html \r\n"+"\r\n" + "404";
    }

    public static String getResponseHeader200(String context){
        return "HTTP/1.1 200 \r\n"+
                "Content-Type:text/html \r\n"+"\r\n" + context;
    }

    public static void main(String[] args) {
        System.out.println("-----");
    }
}
